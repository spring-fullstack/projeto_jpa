package br.com.mrb.projeto_jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mrb.projeto_jpa.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByName(String name);
}