package br.com.mrb.projeto_jpa.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.com.mrb.projeto_jpa.entity.User;
import br.com.mrb.projeto_jpa.repository.UserRepository;

@Component
public class DataInitialize implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		System.out.println("Entrou aqui");
		
		List<User> users = userRepository.findAll();
		
		if(users.isEmpty()) {
			this.createUser("Mauro Braga","mrb0305@gmail.com");
			this.createUser("Vanessa Braga","vanessa@gmail.com");
			this.createUser("Amanda  Braga","amanda@gmail.com");
			this.createUser("Clara Lívia","clara@gmail.com");
		}
//		User user = userRepository.getOne(2L);
//		System.out.println(user.getName());
//		userRepository.deleteById(2L);;
		
//		user = userRepository.getOne(1L);
//		user.setName("Mauro Rodrigues Braga");
//		userRepository.save(user);
		
		
		User user = userRepository.findByName("Mauro Braga");
		System.out.println(user.getName());
	}
	
	public void createUser(String name, String email) {
		User  user = new User(name, email);
		userRepository.save(user);
	}

}
